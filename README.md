## The configdb-server package
Documentation can be found [here](https://demi.docs.cern.ch/pypi-packages/configdb-server/).


## Installation and Setup
### Build & Publish

```shell
 poetry config repositories.configdb-server "https://gitlab.cern.ch/api/v4/projects/131311/packages/pypi/"

 poetry version ...
 
 poetry publish -r configdb-server -u USERNAME -p $(cat ~/.gitlab-token) --build
```

### Alembic
To create a new schema revision use (make sure sqlalchemy.url in alembic.ini is set properly):
```shell
cd configdb_server
alembic revision --autogenerate -m "comment"
```
To apply it use the:
```shell
alembic upgrade head
```
or use the function from the configdb-server package.