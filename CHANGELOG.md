# Changelog

All notable changes[^1] to the `configdb-server`

This python package is used to access the configDB staging area and storage database.
The itk-demo-configdb microservices provide a REST API to this package.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
(always keep this section to take note of untagged changes at HEAD)

## [1.11.0] - 2024-09-24
- Added option to add tags and objects when creating objects or payloads

## [1.10.6] - 2024-09-18
- Small bugfix when reading objects

## [1.10.5] - 2024-09-13
- Enabled search_in_tag method to sent payload_data when order_by_dict

## [1.10.4] - 2024-09-09
- Added connections param to read_all

## [1.10.3] - 2024-09-09
- Added id param to add_to_tree

## [1.10.2] - 2024-09-05
- Added additional options to search_in_tag method

## [1.10.1] - 2024-09-04
- Added keep_ids option to commit and clone

## [1.10.0] - 2024-09-03
- Added search_in_tag method to search for payload from specific object inside a tag
- Enabled bulk search functionality for postgres adapter

## [1.9.1] - 2024-08-29
- Improved db migration status checking

## [1.9.0] - 2024-08-28
- Bugfix in runkey creation when reusing ids
- Added bulk deletion methods
- Improved bulk read methods

## [1.8.0] - 2024-08-27
- Added tag_tag table
- Added members and groups params to tag creation methods
- Now returns these params in tag read methods
- Added members and groups params to dataclasses

## [1.7.6] - 2024-08-22
- Added payload_filter to read_tree methods
- Reenabled committing of tree identified by their root-node

## [1.7.5] - 2024-08-21
- Fixed bug when cloning/committing runkey graphs
- Reenabled sending of payload data as dict

## [1.7.4] - 2024-08-14
- Fixed bug that failed object_insert when given id was None

## [1.7.3] - 2024-08-09
- Fixed bug that prevented using a given id as the uuid in object_insert

## [1.7.2] - 2024-08-06
- Bugfixes for using tags with multiple objects or payloads
- Removed forced lower case of tag names

## [1.7.1] - 2024-08-01
- Moved dataclass definitions to server code

## [1.7.0] - 2024-08-01
- Fixes to work with DataClasses as input. Server code will be changed to work with dataclasses itself in a future release
- Small snytax changes root-id -> id, association_dict -> associations

## [1.6.5] - 2024-07-31
- Added option to filter tag and payload table by dataset name

## [1.6.4] - 2024-07-12
- Check if tag is in use before sending data to database on, create, clone and commit
- Improved NameInUseError message

## [1.6.3] - 2024-07-04
- Added format option to payload read functions, to enable formatting of json payloads

## [1.6.2] - 2024-07-01
- Bugfix in change_connected_payloads method
- Added meta option to read and update payloads (to enable direct queries to the metadata table)

## [1.6.1] - 2024-06-28
- Added payload_update method

## [1.6.0] - 2024-06-27
- Mayor rework of commit and clone methods
    - No dont construct & deconstruct trees anymore
    - Fixed linked payloads being broken up
- Improvements & bugfixes to bulk payload methods

## [1.5.2] - 2024-06-26
- Fixed error message when ID was already in use
- Improvements & bugfixes to bulk payload methods

## [1.5.1] - 2024-06-20
- Bugfix in change_connected_payloads, if index of list is send as string

## [1.5.0] - 2024-06-19
- Added functions to bulk change payloads
    - bundle_payloads and change_connected_payloads

## [1.4.3] - 2024-06-10
- Made new tag optional when committing a runkey

## [1.4.2] - 2024-06-05
- Small improvements to startup functions and errors
- Improved printout of error messages

## [1.4.1] - 2024-05-27
- Bugfixes in add_to_node and tree read operations

## [1.4.0] - 2024-05-16
- Bugfixes for graph operations
    - Breaking change: Added depth to primary key of closure table
- Added update connections function
- Renamed delete to delete_tree
- Fixed delete so that it does only delete orphan payloads
- Changes & Fixes in full runkey creation from a dict
    - Changed key which is used to detect whether a payload/object should be reused to "reuse_id" (was "id")
    - Fixed reusing of subtrees 

## [1.3.1] - 2024-05-15
- Added view to read object
- Bugfixes

## [1.3.0] - 2024-05-14
- Added view column
- Added view parameter to read functions enabling filtering by view
- Added view parameter to write functions to set view of connections
- Reworked bulk_read functions to first query objects that are part of the tree and than the payloads connected to these objects
    - Now uses SQLAlchemy to construct queries (enabling the usage of the same code for all backend types)
- Removed CLI because it is now part of pyconfigdb package

## [1.2.3] - 2024-04-25
- Bugfix in pdb import

## [1.2.2] - 2024-04-25
- Additional options for pdb import

## [1.2.1] - 2024-04-25
- Added serial-number to frontend imported from pdb

## [1.2.0] - 2024-04-23
- Reworked output of search functions

## [1.1.0] - 2024-04-18
- Removed old import / export functions
- Added pdb import functionality

## [1.0.4] - 2024-04-17
- Fixed bug in tag creation, that prevented adding more than one object

## [1.0.3] - 2024-04-11
- Added error handling when reading, cloning or committing empty tags

## [1.0.2] - 2024-04-11
- Fixed alembic overriding logger

## [1.0.1] - 2024-04-09
- Updated dependencies
- Small fixes

## [1.0.0] - 2024-04-09
- Renamed package to configdb-server
- Changed gitlab url

## [0.46.1] - 2024-04-04
### Added
- Improved error handling (added rollbacks to certain errors)
- Fixes to ensure support for older python version (3.9)

## [0.46.0] - 2024-04-04
### Added
- Added export tree method as an equivalent to the import tree method

## [0.45.1] - 2024-04-03
### Added
- Factored out export functionality to additional function

## [0.45.0] - 2024-03-13
### Added
- Added option to reuse payloads when creating a full runkey from a dict 
- Enabled foreign key contraints for SQLite backend

## [0.44.0] - 2024-03-12
### Added
- Parameterized SQL requests to protect against SQL injection attacks

## [0.43.1] - 2024-02-27
### Fixed
- Return tag comment with tag tree methods
### Added
- Option to set comment, author and type when cloning a tag
- Made setting of backend driver optional

## [0.43.0] - 2024-02-27
### Added
- Added postgres adapter class
- Added dependencies for postgres 

## [0.42.4] - 2024-02-21
### Added
- Added comment attribute to additional functions

## [0.42.3] - 2024-02-21
### Added
- Made tagging newest tag with "latest" optional

## [0.42.2] - 2024-02-21
### Fixes
- Error in migration if some tables already existed

## [0.42.1] - 2024-02-21
### Fixes
- Include migrations in package

## [0.42.0] - 2024-02-21
### Added
- Added alembic for database migrations
- Added comment attribute to tag table and its functions
- Added latest tag that always points to the newest tag

## [0.41.1] - 2024-02-15
### Fixes
- Bugfixes for new backend and profiling

## [0.41.0] - 2024-02-15
### Added
- Added new mariadb backend, which uses mysql-connector instead of sqlalchemy-core for bulk inserts and queries

## [0.40.1] - 2024-02-14
### Added
- Added type of profile to enable auto-completion in IDE

## [0.40.0] - 2024-02-14
### Added
- Added profiling class and functions

## [0.39.0] - 2024-01-22
### Added
- Added option to chose whether to encode/decode on many read/write operations

## [0.38.2] - 2024-01-18
### Fixed
- Fixed bug in search function, nested dicts are now formatted correctly
- Fixed bug in search function that caused the search to only use one key-value pair even if multiple were sent

## [0.38.1] - 2024-01-18
### Fixed
- Fixed bug in logging that would output certain lines multiple times

## [0.38.0] - 2024-01-09
### Changed
- Reworked custom error ids

## [0.37.1] - 2024-01-08
### Changed
- Import: Type of configs now is based on filename

## [0.37.0] - 2023-12-19
### Added
- Functions to create runkey dict from directory structure
- Function that imports directory structure runkey into the database

## [0.36.0] - 2023-12-15
### Changed
- Session functionality
    - renamend get_session to create_session
    - removed close_session, use session.close() instead
### Added
- Search object and search subtree functions

## [0.35.10] - 2023-12-13
### Fixed
- Changed tag model to return metadata as a dict (smiliar to objects)

## [0.35.5] - 2023-12-08
### Fixed
- Fixed encoding error if payload was send as json (instead of string)

## [0.35.4] - 2023-12-08
### Fixed
- config search for mariadb backend now returns empty list if runkey but no objects were found

## [0.35.3] - 2023-11-29
### Added
- Pytests for mariadb backend (via mariadbs run in docker container)

### Fixed
- To few results for search without search_dict for mariadb backend

## [0.35] - 2023-11-29
### Changed
- Distinction between metadata and payload is now done via boolean parameter
    not implicitely via the type of the dataset

## [0.34] - 2023-11-29
### Changed
- Metadata is returned as formatted json

## [0.33] - 2023-11-28
### Added
- Improved error handling for dict errors

## [0.32] - 2023-11-24

### Added
- Started changelog

### Removed

### Changed
- Changed behaviour of config and object filter options for searches
    - Filter now only have to be part of the config or object type to be found