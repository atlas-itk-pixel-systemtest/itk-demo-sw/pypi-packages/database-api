from tests.sqlalchemy.conftest import Fixture
from configdb_server.testing_tools import extract_dict, sort_list, format_tree_data
from configdb_server.exceptions import EmptyTagError

import pytest


def test_get_tree_object(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    read_session = db.backend.create_read_session()
    tag = db.backend.read_tag(read_session, "root_tag")
    read_session.close()
    id = tag["objects"][0]

    tree = db.read_tree(id, payload_data=True)

    input = extract_dict(data)
    output = extract_dict(tree)

    assert sort_list(input) == sort_list(output)


def test_get_tree_tag(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    tree = db.read_tree("root_tag", payload_data=True)

    input = extract_dict(data)
    output = extract_dict(tree)

    assert sort_list(input) == sort_list(output)


def test_get_tree_tag_filter(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    tree = db.read_tree("root_tag", payload_data=True, payload_filter="frontend")

    output = extract_dict(tree)
    sort = sort_list(output)

    assert len(sort[1]) == 0
    assert len(sort[0][1]) == 0
    assert len(sort[0][2]) == 0
    assert len(sort[0][0][0]) == 1
    assert len(sort[0][0][1]) == 1


def test_get_tree_empty_tag(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, "empty_tag", "runkey")
    write_session.close()

    with pytest.raises(EmptyTagError):
        db.read_tree("empty_tag", payload_data=True)


def test_create_tag_comment(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    tree = db.read_tree("root_tag", payload_data=True)

    write_session = db.backend.create_write_session()
    db.backend.create_tag(write_session, "my_tag", "runkey", objects=[tree["id"]], comment="test", author="pytest")
    tag = db.backend.read_tag_tree(write_session, "my_tag")
    write_session.close()

    output = extract_dict(tag)
    input = extract_dict(data)

    assert tag["comment"] == "test"
    assert tag["author"] == "pytest"


def test_get_tree_tag_meta(connection_scan_data: Fixture):
    db = connection_scan_data.stage_db
    data = connection_scan_data.data

    tree = db.read_tree("root_tag", payload_data=True)

    input = extract_dict(data)
    output = extract_dict(tree)

    sorted_input = sort_list(input)
    sorted_output = sort_list(output)

    assert sorted_input == sorted_output

    for child in tree["children"]:
        if child["type"] == "scan":
            assert child["metadata"] == {"scan_type": "digitalscan", "time": "2023-07-12 07:16:36", "author": "testing_tools"}


# def test_get_tree_tag_meta_no_payload(connection_scan_data: Fixture):
#     db = connection_scan_data.stage_db
#     data = connection_scan_data.data

#     tree = db.read_tree("root_tag", payload_data=False)

#     input = extract_dict(data, remove_data=True)
#     output = extract_dict(tree)

#     sorted_input = sort_list(input)
#     sorted_output = sort_list(output)

#     assert sorted_input == sorted_output

#     for child in tree["children"]:
#         if child["type"] == "scan":
#             assert child["metadata"] == {"scan_type": "digitalscan", "time": "2023-07-12 07:16:36", "author": "testing_tools"}


def test_get_tree_tag_no_data(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    tree = db.read_tree("root_tag")

    input = extract_dict(data, remove_data=True)
    output = extract_dict(tree)

    assert sort_list(input) == sort_list(output)


def test_get_tree_tag_depth(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    tree = db.read_tree("root_tag", payload_data=True, depth=2)

    input = extract_dict(data, remove_type="frontend")
    output = extract_dict(tree)

    assert sort_list(input) == sort_list(output)


def test_get_tree_tag_members(connection_member_data: Fixture):
    db = connection_member_data.stage_db
    data = connection_member_data.data

    session = db.backend.create_read_session()
    tree = db.backend.read_tag_tree(session, "bundle", payload_data=True)
    session.close()

    input = extract_dict(data)
    output = extract_dict(tree["members"][0]["objects"][0])

    assert sort_list(input) == sort_list(output)
    assert len(tree["members"]) == 2


def test_format_tree(connection_small_data: Fixture):
    db = connection_small_data.stage_db

    tree = db.read_tree("root_tag", payload_data=True)
    text = db.format_tree(tree, include_id=False)

    assert text == format_tree_data
