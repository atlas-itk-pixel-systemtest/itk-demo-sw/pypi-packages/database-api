from tests.sqlalchemy.conftest import Fixture
from configdb_server.adapter.base_adapter import Connection
from configdb_server.testing_tools import extract_dict, sort_list
from json import dumps

import pytest


def test_update_object(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    object_id = db.backend.create_object(write_session, "root")
    child_id = db.backend.create_object(write_session, "test_child")
    payload_id = db.backend.create_payload(write_session, "test_payload", "test_data")
    metadata_id = db.backend.create_payload(write_session, "metadata", {"test": "test"}, meta=True)

    db.backend.add_to_object(write_session, object_id, [Connection(child_id)], [payload_id, metadata_id])
    new_object = db.read_tree(object_id, payload_data=True)

    db.backend.remove_from_object(
        write_session,
        object_id,
        [child_id],
        [metadata_id, payload_id],
    )
    write_session.close()

    empty_object = db.read_tree(object_id)

    assert not empty_object["payloads"]
    assert not empty_object["children"]

    i = 0
    if new_object["payloads"][0]["type"] == "metadata":
        i = i + 1
    assert new_object["payloads"][i]["type"] == "test_payload"
    assert new_object["children"][0]["type"] == "test_child"

    assert len(new_object["payloads"]) == 2
    assert len(new_object["children"]) == 1


def test_update_tag(connection_data: Fixture):
    db = connection_data.stage_db

    read_session = db.backend.create_read_session()
    write_session = db.backend.create_write_session()
    tag = db.backend.read_tag_tree(read_session, "root_tag")
    tag2 = db.backend.create_tag(write_session, "member", "runkey")
    tag3 = db.backend.create_tag(write_session, "group", "runkey")

    res = db.backend.add_to_tag(
        write_session,
        "root_tag",
        objects=[tag["objects"][0]["children"][0]["id"]],
        payloads=[tag["objects"][0]["payloads"][0]["id"], tag["objects"][0]["payloads"][1]["id"]],
        members=["member"],
        groups=["group"],
    )
    db.backend.remove_from_tag(write_session, "root_tag", [tag["objects"][0]["id"]])
    new_tag = db.backend.read_tag_tree(read_session, "root_tag")

    res = db.backend.remove_from_tag(
        write_session,
        "root_tag",
        objects=[new_tag["objects"][0]["id"]],
        payloads=[new_tag["payloads"][0]["id"], new_tag["payloads"][1]["id"]],
        members=["member"],
        groups=["group"],
    )

    read_session.close()
    read_session = db.backend.create_read_session()
    empty_tag = db.backend.read_tag_tree(read_session, "root_tag")
    read_session.close()
    write_session.close()

    assert not empty_tag["payloads"]
    assert not empty_tag["objects"]
    assert not empty_tag["members"]
    assert not empty_tag["groups"]

    assert new_tag["members"][0]["name"] == "member"
    assert new_tag["groups"][0] == "group"

    assert new_tag["payloads"][0] == tag["objects"][0]["payloads"][0] or new_tag["payloads"][1] == tag["objects"][0]["payloads"][0]
    assert new_tag["payloads"][1] == tag["objects"][0]["payloads"][1] or new_tag["payloads"][0] == tag["objects"][0]["payloads"][1]
    new_tag["objects"][0]["view"] = 1
    assert new_tag["objects"][0]["id"] == tag["objects"][0]["children"][0]["id"]

    output = extract_dict(new_tag["objects"][0])
    input = extract_dict(tag["objects"][0]["children"][0])

    assert sort_list(input) == sort_list(output)


def test_update_connection(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    object_id = db.backend.create_object(write_session, "root")
    child_id = db.backend.create_object(write_session, "test_child")
    payload_id = db.backend.create_payload(write_session, "test_payload", "test_data")
    metadata_id = db.backend.create_payload(write_session, "metadata", {"test": "test"}, meta=True)

    db.backend.add_to_object(write_session, object_id, [Connection(child_id)], [payload_id, metadata_id])
    tree = db.read_tree(object_id, payload_data=True)

    db.backend.remove_from_object(write_session, object_id, [child_id])

    empty_tree = db.read_tree(object_id)

    db.backend.update_connections(write_session, [{"parent": object_id, "child": child_id, "view": 1}])
    write_session.close()

    new_tree = db.read_tree(object_id, payload_data=True)

    assert not empty_tree["children"]

    assert tree == new_tree


def test_update_payload(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    payload_id = db.backend.create_payload(write_session, "test_type", "test_data", "test_name")
    assert db.backend.update_payload(write_session, payload_id, "test_type2", "test_data2", "test_name2")
    payload = db.backend.read_payload(write_session, payload_id)
    write_session.close()

    assert payload["type"] == "test_type2"
    assert payload["data"] == "test_data2"
    assert payload["name"] == "test_name2"
    assert payload["id"] == payload_id


def test_update_metadata(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    payload_id = db.backend.create_payload(write_session, "test_type", dumps({"test": "data"}), "test_name", meta=True)
    assert db.backend.update_payload(write_session, payload_id, "test_type2", dumps({"test": "data2"}), "test_name2", meta=True)
    payload = db.backend.read_payload(write_session, payload_id, format=True)
    write_session.close()

    assert payload["type"] == "test_type2"
    assert payload["data"] == '{\n    "test": "data2"\n}'
    assert payload["name"] == "test_name2"
    assert payload["id"] == payload_id
