from tests.sqlalchemy.conftest import Fixture
from configdb_server.testing_tools import extract_dict, sort_list, std_data, std_data_small_existing_payload
from configdb_server.adapter.base_adapter import Connection
from configdb_server.exceptions import NotValidUUIDError, DatasetNotFoundError, LoopError
from uuid import uuid4
import copy
import pytest
import json


def test_runkey_from_dict(connection_data: Fixture):
    db = connection_data.stage_db
    data = connection_data.data

    tree = db.read_tree("root_tag", payload_data=True)

    output = extract_dict(tree)
    input = extract_dict(data)

    assert sort_list(input) == sort_list(output)


def test_runkey_with_existing_payload(connection: Fixture):
    db = connection.stage_db

    db.create_full_tree(std_data_small_existing_payload, "root_tag")
    tree = db.read_tree("root_tag", payload_data=True)

    assert tree["payloads"][0] == tree["children"][0]["payloads"][0]


def test_runkey_id_not_exist(connection: Fixture):
    db = connection.stage_db

    data = copy.deepcopy(std_data_small_existing_payload)
    uuid = uuid4().hex
    data["children"][0]["payloads"][0]["reuse_id"] = uuid

    with pytest.raises(DatasetNotFoundError):
        db.create_full_tree(data, "root_tag")


def test_runkey_wrong_id_format(connection: Fixture):
    db = connection.stage_db

    data = copy.deepcopy(std_data_small_existing_payload)
    data["payloads"][0]["id"] = "1"

    with pytest.raises(NotValidUUIDError):
        db.create_full_tree(data, "root_tag")


def test_runkey_manual(connection: Fixture):
    db = connection.db

    root_uuid = db.create_root(
        "root_tag",
        payloads=[
            {"data": "root_node payload", "type": "config", "name": "test"},
            {"data": "connectivity data", "type": "connect", "name": "test"},
        ],
    )
    felix1_uuid = db.add_to_tree(
        "felix",
        parents=[Connection(root_uuid)],
        payloads=[
            {"data": "payload_felix01", "type": "felix", "name": "test"},
            {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
        ],
    )
    db.add_to_tree("felix", parents=[Connection(root_uuid)], payloads=[{"data": "payload_felix02", "type": "felix", "name": "test"}])
    lpgbt1_uuid = db.add_to_tree(
        "lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}]
    )
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe01", "type": "frontend", "name": "test"}])
    db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe02", "type": "frontend", "name": "test"}])

    tree = db.read_tree("root_tag", payload_data=True)

    output = extract_dict(tree)
    input = extract_dict(std_data)

    assert sort_list(input) == sort_list(output)


def test_add_node(connection: Fixture):
    db = connection.db

    root_uuid = db.create_root("root_tag")
    felix1_uuid = db.add_to_tree(
        "felix", parents=[Connection(root_uuid)], payloads=[{"data": "payload_felix01", "type": "felix", "name": "test"}]
    )
    felix2_uuid = db.add_to_tree(
        "felix", parents=[Connection(root_uuid)], payloads=[{"data": "payload_felix02", "type": "felix", "name": "test"}]
    )
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid), Connection(felix2_uuid)])

    tree = db.read_tree("root_tag", payload_data=True)

    assert tree["children"][0]["children"][0]["id"] == tree["children"][1]["children"][0]["id"]


def test_recursive_closure(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    uuid0 = db.backend.create_object(write_session, "0")

    uuid1 = db.backend.create_object(write_session, "1", parents=[Connection(uuid0)])
    uuid2 = db.backend.create_object(write_session, "2", parents=[Connection(uuid1)])
    uuid4 = db.backend.create_object(write_session, "4")
    uuid5 = db.backend.create_object(write_session, "5", parents=[Connection(uuid4)])

    uuid3 = db.backend.create_object(write_session, "3", parents=[Connection(uuid2)], children=[Connection(uuid4)])

    write_session.close()

    read_session = db.backend.create_read_session()
    tree = db.backend.read_object_tree(read_session, uuid0)
    read_session.close()

    assert tree["type"] == "0"
    assert tree["children"][0]["type"] == "1"
    assert tree["children"][0]["children"][0]["type"] == "2"
    assert tree["children"][0]["children"][0]["children"][0]["type"] == "3"
    assert tree["children"][0]["children"][0]["children"][0]["children"][0]["type"] == "4"
    assert tree["children"][0]["children"][0]["children"][0]["children"][0]["children"][0]["type"] == "5"


def test_runkey_manual_bulk(connection: Fixture):
    db = connection.db

    root_uuid = db.create_root(
        "root_tag",
        payloads=[
            {"data": "root_node payload", "type": "config", "name": "test"},
            {"data": "connectivity data", "type": "connect", "name": "test"},
        ],
    )
    felix1_uuid = db.add_to_tree(
        "felix",
        parents=[Connection(root_uuid)],
        payloads=[
            {"data": "payload_felix01", "type": "felix", "name": "test"},
            {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
        ],
    )
    db.add_to_tree("felix", parents=[Connection(root_uuid)], payloads=[{"data": "payload_felix02", "type": "felix", "name": "test"}])
    lpgbt1_uuid = db.add_to_tree(
        "lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}]
    )
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("lpgbt", parents=[Connection(felix1_uuid)], payloads=[{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}])
    db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe01", "type": "frontend", "name": "test"}])
    db.add_to_tree("frontend", parents=[Connection(lpgbt1_uuid)], payloads=[{"data": "payload_fe02", "type": "frontend", "name": "test"}])

    tree = db.read_tree("root_tag", payload_data=True)

    output = extract_dict(tree)
    input = extract_dict(std_data)

    assert sort_list(input) == sort_list(output)


def test_loop(connection: Fixture):
    db = connection.db

    write_session = db.backend.create_write_session()
    object_id = db.backend.create_object(write_session, "root")
    child_id = db.backend.create_object(write_session, "test_child")
    grandchild_id = db.backend.create_object(write_session, "test_grandchild")

    db.backend.add_to_object(write_session, object_id, [Connection(child_id)])
    db.backend.add_to_object(write_session, child_id, [Connection(grandchild_id)])

    with pytest.raises(LoopError):
        db.backend.add_to_object(write_session, grandchild_id, [Connection(object_id)])


def test_runkey_with_payload(connection: Fixture):
    db = connection.stage_db

    write_session = db.backend.create_write_session()
    payl_id = db.backend.create_payload(write_session, "test_payload", data=json.dumps({"test": "test"}), meta=True)
    db.create_full_tree(std_data, "root_tag", payloads=[payl_id])
    tree = db.backend.read_tag_tree(write_session, "root_tag", payload_data=True)
    write_session.close()

    assert tree["metadata"]["test"] == "test"
