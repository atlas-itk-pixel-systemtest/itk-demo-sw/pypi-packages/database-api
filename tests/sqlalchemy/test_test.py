from tests.sqlalchemy.conftest import Fixture

import pytest


def test_rk(connection: Fixture):
    stage_db = connection.stage_db

    stage_db.create_full_tree(runkey_dict, "root_tag")
    tree = stage_db.read_tree("root_tag", payload_data=True)

runkey_dict =  { 
  "type": "Root1",
  "children": [
    {
      "type": "Root",
      "children": [
        {
          "type": "Felix",
          "payloads": [
            {
              "type": "config",
              "data": "felix_config_file"
            }
          ],
          "children": []
        }
      ]
    },
    {
      "type": "Root",
      "children": [
        {
          "type": "Felix",
          "payloads": [
            {
              "type": "config",
              "data": "felix_config_file"
            }
          ],
          "children": []
        }
      ]
    }
  ]
}