from tests.sqlalchemy.conftest import Fixture
from configdb_server.exceptions import NotValidJSonError, DatasetNotFoundError

import pytest, json


def test_meta_invalid_json(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    session.close()

    with pytest.raises(NotValidJSonError):
        db.backend.create_payload(session, "meta", "test", meta=True)


def test_payload(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()

    id1 = db.backend.create_payload(session, "test", '{"test": 1}', "test")
    id2 = db.backend.create_payload(session, "meta", '{"test": 1}', "test", meta=True)

    payl1 = db.backend.read_payload(session, id1)
    payl2 = db.backend.read_payload(session, id2)

    session.close()

    assert json.loads(payl1["data"]) == json.loads(payl2["data"])


def test_metadata_json(connection: Fixture):
    db = connection.stage_db
    data = '{\n    "felix_app": 1,\n    "felix_card_number": 0,\n    "felix_device_number": 0,\n    "felix_initialize": 3,\n    "dryrun": 0,\n    "noflx": 0,\n    "felix_config_file": "felix_config.json",\n    "felix_data_interface": "lo",\n    "felix_toflx_ip": "localhost",\n    "felix_tohost_ip": "localhost",\n    "felix_tohost_dcs_pages": 256,\n    "serial": 123\n}'

    session = db.backend.create_write_session()

    id = db.backend.create_payload(session, "meta", data, "test", meta=True)
    payl = db.backend.read_payload(session, id, format=True)
    session.close()

    assert json.loads(payl["data"]) == json.loads(data)


def test_payload_not_found(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()

    id = db.backend.create_payload(session, "payl", "test_data")
    session.close()

    with pytest.raises(DatasetNotFoundError):
        db.backend.read_payload(session, id, meta=True)


def test_payload_not_found2(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    id = db.backend.create_payload(session, "payl", '{"a":1}', meta=True)
    payl = db.backend.read_payload(session, id)
    session.close()

    assert payl["data"] == '{"a": 1}'


def test_payload_no_format(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    id = db.backend.create_payload(session, "payl", "test_data", "test")
    payl = db.backend.read_payload(session, id, format=True)
    session.close()

    assert payl["data"] == "test_data"


def test_payload_format(connection: Fixture):
    db = connection.stage_db
    data = json.dumps({"a": 1})

    session = db.backend.create_write_session()

    id = db.backend.create_payload(session, "meta", data, "test")
    payl = db.backend.read_payload(session, id, format=True)
    session.close()

    assert payl["data"] == json.dumps({"a": 1}, indent=4)


def test_all(connection: Fixture):
    db = connection.stage_db

    session = db.backend.create_write_session()
    db.backend.create_tag(session, "test_tag", "test")
    obj_id = db.backend.create_object(session, "test_obj", tags=["test_tag"])
    db.backend.create_payload(session, "test_payl", "test_data", objects=[obj_id], tags=["test_tag"])
    db.backend.create_payload(session, "meta", '{"a": 1}', "test", objects=[obj_id], tags=["test_tag"], meta=True)

    tag = db.backend.read_tag_tree(session, "test_tag")
    object = db.read_tree(obj_id)

    session.close()

    assert len(object["payloads"]) == 2
    assert len(tag["payloads"]) == 2
    assert len(tag["objects"]) == 1
