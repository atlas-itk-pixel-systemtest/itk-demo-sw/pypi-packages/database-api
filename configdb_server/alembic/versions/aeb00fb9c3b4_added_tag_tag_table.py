"""added tag_tag table

Revision ID: aeb00fb9c3b4
Revises: 818a1251d4c2
Create Date: 2024-08-23 10:40:52.451532

"""

from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
import sqlalchemy_utils

# revision identifiers, used by Alembic.
revision: str = "aeb00fb9c3b4"
down_revision: Union[str, None] = "818a1251d4c2"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "tag_tag",
        sa.Column("member_id", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column("group_id", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.ForeignKeyConstraint(["member_id"], ["tag.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["group_id"], ["tag.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("member_id", "group_id"),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("tag_tag")
    # ### end Alembic commands ###
