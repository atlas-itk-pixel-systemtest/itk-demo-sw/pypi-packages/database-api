import copy, json
from uuid import uuid4


def flatten_list(lst):
    """
    Flatten a nested list into a single list
    """
    result = []
    for item in lst:
        if isinstance(item, list):
            result.extend(flatten_list(item))
        else:
            result.append(item)
    return result


def pop_ids(dct):
    """
    Remove all ids from unnested dict
    """
    dct.pop("id", "")
    for dataset in dct["payloads"]:
        dataset.pop("id", "")
    if "children" in dct:
        for dataset in dct["children"]:
            dataset.pop("id", "")
    return dct


def sort_list(lst):
    """
    Sort nested lists
    """
    for i in range(len(lst)):
        if isinstance(lst[i], dict):
            lst[i] = dict(sorted(lst[i].items()))
        if isinstance(lst[i], list):
            lst[i] = sort_list(lst[i])
    return sorted(lst, key=lambda x: str(x))


def extract_dict(data, remove_data=False, remove_type=""):
    """
    Extract data from dict
    """
    output = copy.deepcopy(data)
    payloads = []
    for dataset in output["payloads"]:
        dataset.pop("id", "")
        dataset.pop("meta", "")
        dataset.pop("view", "")
        if remove_data:
            dataset.pop("data", "")
        else:
            if "data" in dataset:
                try:
                    dataset["data"] = json.loads(dataset["data"])
                except:
                    pass
        payloads.append(dataset)
    if "children" in output:
        for dataset in output["children"]:
            if dataset["type"] != remove_type:
                sub_list = extract_dict(dataset, remove_data, remove_type)
                payloads.append(sub_list)
    return payloads


def extract_list(output):
    """
    Extract data from list
    """
    payloads = []
    for dataset in output:
        dataset.pop("id")
        dataset.pop("meta", "")
        if "data" in dataset:
            try:
                dataset["data"] = json.loads(dataset["data"])
            except:
                pass
        payloads.append(dataset)

    return payloads


std_data = {
    "type": "root",
    "payloads": [
        {"data": "root_node payload", "type": "config", "name": "test"},
        {"data": "connectivity data", "type": "connect", "name": "test"},
    ],
    "children": [
        {
            "type": "felix",
            "payloads": [
                {"data": "payload_felix01", "type": "felix", "name": "test"},
                {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
            ],
            "children": [
                {
                    "type": "optoboard",
                    "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}],
                    "children": [
                        {
                            "type": "frontend",
                            "payloads": [
                                {
                                    "data": "payload_fe01",
                                    "type": "frontend",
                                    "name": "test",
                                }
                            ],
                        },
                        {
                            "type": "frontend",
                            "payloads": [
                                {
                                    "data": "payload_fe02",
                                    "type": "frontend",
                                    "name": "test",
                                }
                            ],
                        },
                    ],
                },
                {
                    "type": "optoboard",
                    "payloads": [
                        {"data": "payload_lpgbt02", "type": "lpgbt", "name": "test"},
                    ],
                },
                {
                    "type": "optoboard",
                    "payloads": [{"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"}],
                },
            ],
        },
        {
            "type": "felix",
            "payloads": [{"data": "payload_felix02", "type": "felix", "name": "test"}],
        },
    ],
}

std_data_meta = {
    "type": "root",
    "payloads": [
        {"data": "root_node payload", "type": "config", "name": "test"},
        {"data": "connectivity data", "type": "connect", "name": "test"},
    ],
    "children": [
        {
            "type": "felix",
            "payloads": [
                {"data": "payload_felix01", "type": "felix", "name": "test"},
                {"data": "additional_payload_felix01", "type": "felix", "name": "test"},
            ],
            "children": [
                {
                    "type": "optoboard",
                    "payloads": [
                        {"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"},
                        {
                            "type": "meta",
                            "meta": True,
                            "data": '{"serial": "100", "test": "test"}',
                            "name": "test_meta",
                        },
                    ],
                    "children": [
                        {
                            "type": "frontend",
                            "payloads": [
                                {
                                    "data": "payload_fe01",
                                    "type": "frontend",
                                    "name": "test",
                                }
                            ],
                        },
                        {
                            "type": "frontend",
                            "payloads": [
                                {
                                    "data": "payload_fe02",
                                    "type": "frontend",
                                    "name": "test",
                                }
                            ],
                        },
                    ],
                },
                {
                    "type": "optoboard",
                    "payloads": [
                        {"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"},
                        {
                            "type": "meta",
                            "meta": True,
                            "data": '{"serial": "100", "test": {"test": {"test": "content"}}}',
                            "name": "test_meta",
                        },
                    ],
                    "children": [
                        {
                            "type": "frontend",
                            "payloads": [
                                {
                                    "data": "payload_fe01",
                                    "type": "frontend",
                                    "name": "test",
                                }
                            ],
                        },
                        {
                            "type": "frontend",
                            "payloads": [
                                {
                                    "data": "payload_fe02",
                                    "type": "frontend",
                                    "name": "test",
                                }
                            ],
                        },
                    ],
                },
                {
                    "type": "optoboard",
                    "payloads": [
                        {"data": "payload_lpgbt03", "type": "lpgbt", "name": "test"},
                        {
                            "type": "meta",
                            "meta": True,
                            "data": '{"serial": "123", "test": "test"}',
                            "name": "test_meta",
                        },
                    ],
                },
            ],
        },
        {
            "type": "felix",
            "payloads": [{"data": "payload_felix02", "type": "felix", "name": "test"}],
        },
    ],
}

std_data_small = {
    "type": "root",
    "payloads": [{"data": "root_node payload", "type": "config", "name": "test"}],
    "children": [
        {
            "type": "felix",
            "payloads": [
                {"data": "payload_felix01", "type": "felix", "name": "test"},
            ],
            "children": [
                {
                    "type": "lpgbt",
                    "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}],
                    "children": [
                        {
                            "type": "frontend",
                            "payloads": [
                                {
                                    "data": "payload_fe01",
                                    "type": "frontend",
                                    "name": "test",
                                }
                            ],
                        },
                    ],
                }
            ],
        }
    ],
}

uuid = uuid4().hex
std_data_small_existing_payload = {
    "type": "root",
    "payloads": [{"data": "root_node payload", "type": "config", "name": "test", "id": uuid}],
    "children": [
        {
            "type": "felix",
            "payloads": [
                {"reuse_id": uuid},
            ],
            "children": [
                {
                    "type": "lpgbt",
                    "payloads": [{"data": "payload_lpgbt01", "type": "lpgbt", "name": "test"}],
                    "children": [
                        {
                            "type": "frontend",
                            "payloads": [
                                {
                                    "data": "payload_fe01",
                                    "type": "frontend",
                                    "name": "test",
                                }
                            ],
                        },
                    ],
                }
            ],
        }
    ],
}

scan_data = [
    {
        "type": "scan",
        "payloads": [
            {
                "data": '{"scan_type": "digitalscan", "time": "2023-07-12 07:16:36", "author": "testing_tools"}',
                "type": "metadata_scan",
                "name": "meta",
                "meta": True,
            },
            {"data": "scan_config data", "type": "scan_config", "name": "std_scan"},
        ],
        "children": [
            {
                "type": "result",
                "payloads": [{"data": "histogram data", "type": "histogram", "name": "histo"}],
            },
            {
                "type": "result",
                "payloads": [{"data": "histogram data 2", "type": "histogram", "name": "histo"}],
            },
        ],
    },
    {
        "type": "scan",
        "payloads": [
            {
                "data": '{"scan_type": "digitalscan", "time": "2023-07-12 07:16:36", "author": "testing_tools"}',
                "type": "metadata_scan",
                "name": "meta",
                "meta": True,
            },
            {"data": "scan_config data", "type": "scan_config", "name": "std_scan"},
        ],
        "children": [
            {
                "type": "result",
                "payloads": [{"data": "histogram data", "type": "histogram", "name": "histo"}],
            },
            {
                "type": "result",
                "payloads": [{"data": "histogram data 2", "type": "histogram", "name": "histo"}],
            },
        ],
    },
]

std_data_scan = copy.deepcopy(std_data)
std_data_scan["children"].extend(scan_data)

import_data = [
    [
        {
            "type": "connectivity",
            "data": {
                "chipType": "RD53B",
                "chips": [
                    {
                        "config": "./0x15417_L2_1280_warm.json",
                        "tx": 2,
                        "rx": 4,
                        "enable": 0,
                        "locked": 1,
                    },
                    {
                        "config": "./0x15418_L2_1280_warm.json",
                        "tx": 2,
                        "rx": 0,
                        "enable": 0,
                        "locked": 1,
                    },
                    {
                        "config": "./0x15426_L2_1280_warm.json",
                        "tx": 2,
                        "rx": 12,
                        "enable": 0,
                        "locked": 1,
                    },
                    {
                        "config": "./0x15427_L2_1280_warm.json",
                        "tx": 2,
                        "rx": 16,
                        "enable": 1,
                        "locked": 1,
                    },
                ],
            },
            "name": "connect_warm.json",
        },
        {
            "type": "felix",
            "data": {
                "ctrlCfg": {
                    "type": "Netio",
                    "cfg": {
                        "NetIO": {
                            "host": "127.0.0.1",
                            "txport": 12340,
                            "rxport": 12350,
                            "manchester": False,
                            "flip": False,
                            "extend": False,
                            "fetype": "rd53b",
                        }
                    },
                }
            },
            "name": "controller.json",
        },
        [
            {
                "type": "frontend",
                "data": {"testdata": "test"},
                "name": "0x15426_L2_1280_warm.json",
            }
        ],
        [
            {
                "type": "frontend",
                "data": {"testdata": "test"},
                "name": "0x15417_L2_1280_warm.json",
            }
        ],
        [
            {
                "type": "frontend",
                "data": {"testdata": "test"},
                "name": "0x15418_L2_1280_warm.json",
            }
        ],
        [
            {
                "type": "frontend",
                "data": {"testdata": "test"},
                "name": "0x15427_L2_1280_warm.json",
            }
        ],
    ]
]

import_data_tree = [
    {
        "type": "connectivity_default",
        "name": "connectivity_default.json",
        "data": {
            "connectivity": {
                "host": "127.0.0.1",
                "cmd_port": 12350,
                "data_port": 12360,
                "enable": 1,
                "locked": 0,
            }
        },
    },
    [
        {
            "type": "felix_meta",
            "name": "felix_meta.json",
            "data": {
                "felix_app": 1,
                "felix_card_number": 0,
                "felix_device_number": 0,
                "felix_initialize": 3,
                "dryrun": 0,
                "noflx": 0,
                "felix_config_file": "felix_config.json",
                "felix_data_interface": "lo",
                "felix_toflx_ip": "localhost",
                "felix_tohost_ip": "localhost",
                "felix_tohost_dcs_pages": 256,
                "serial": 123,
            },
        },
        {
            "type": "felix_config",
            "name": "felix_config.json",
            "data": {
                "FelixID": 0,
                "DeviceID": 0,
                "Links": {"0": {"polarity": 1, "icec": 1, "rx": [0], "tx": [0]}},
            },
        },
        [
            {
                "type": "opto_meta",
                "name": "opto_meta.json",
                "data": {
                    "serial": 301,
                    "vtrx_v": "1.3",
                    "ConfigurationDB": "http://localhost:5000",
                    "config_path": "/usr/local/lib/python3.9/site-packages/optoboard_felix/configs/optoboard2_lpgbtv1_gbcr2_vtrxv1_3_default.json",
                    "debug": False,
                    "flx_G": "0",
                    "flx_d": "0",
                    "configInDB": False,
                    "test_mode": True,
                    "commToolName": "ic-over-netio",
                },
            },
            {
                "type": "opto_config",
                "name": "opto_config.json",
                "data": {
                    "Optoboard": {
                        "serial": "V2_008",
                        "version": 2,
                        "config_mode": "IC",
                        "FELIX": 1,
                        "link_group": 0,
                        "FPGA_USB_port": 0,
                        "PSU_USB_port": 1,
                        "debug": 0,
                        "I2C_master": 2,
                        "I2C_set": 1,
                        "SCLDriveMode": 0,
                        "FREQ": 2,
                    }
                },
            },
            [
                {
                    "type": "chip1_random",
                    "name": "chip1_random.json",
                    "data": {"RD53A": 1},
                }
            ],
            [
                {
                    "type": "chip1_random",
                    "name": "chip1_random.json",
                    "data": {"RD53A": 1},
                }
            ],
        ],
        [
            {
                "type": "opto_meta",
                "name": "opto_meta.json",
                "data": {
                    "serial": 300,
                    "vtrx_v": "1.3",
                    "ConfigurationDB": "http://localhost:5000",
                    "config_path": "/usr/local/lib/python3.9/site-packages/optoboard_felix/configs/optoboard2_lpgbtv1_gbcr2_vtrxv1_3_default.json",
                    "debug": False,
                    "flx_G": "0",
                    "flx_d": "0",
                    "configInDB": False,
                    "test_mode": True,
                    "commToolName": "ic-over-netio",
                },
            },
            {
                "type": "opto_config",
                "name": "opto_config.json",
                "data": {
                    "Optoboard": {
                        "serial": "V2_008",
                        "version": 2,
                        "config_mode": "IC",
                        "FELIX": 1,
                        "link_group": 0,
                        "FPGA_USB_port": 0,
                        "PSU_USB_port": 1,
                        "debug": 0,
                        "I2C_master": 2,
                        "I2C_set": 1,
                        "SCLDriveMode": 0,
                        "FREQ": 2,
                    }
                },
            },
            [
                {
                    "type": "chip1_random",
                    "name": "chip1_random.json",
                    "data": {"RD53A": 1},
                }
            ],
            [
                {
                    "type": "chip1_random",
                    "name": "chip1_random.json",
                    "data": {"RD53A": 1},
                }
            ],
        ],
    ],
    [
        {
            "type": "felix_meta",
            "name": "felix_meta.json",
            "data": {
                "felix_app": 1,
                "felix_card_number": 0,
                "felix_device_number": 0,
                "felix_initialize": 3,
                "dryrun": 0,
                "noflx": 0,
                "felix_config_file": "felix_config.json",
                "felix_data_interface": "lo",
                "felix_toflx_ip": "localhost",
                "felix_tohost_ip": "localhost",
                "felix_tohost_dcs_pages": 256,
                "serial": 124,
            },
        },
        {
            "type": "felix_config",
            "name": "felix_config.json",
            "data": {
                "FelixID": 0,
                "DeviceID": 0,
                "Links": {"0": {"polarity": 1, "icec": 1, "rx": [0], "tx": [0]}},
            },
        },
        [
            {
                "type": "opto_meta",
                "name": "opto_meta.json",
                "data": {
                    "serial": 302,
                    "vtrx_v": "1.3",
                    "ConfigurationDB": "http://localhost:5000",
                    "config_path": "/usr/local/lib/python3.9/site-packages/optoboard_felix/configs/optoboard2_lpgbtv1_gbcr2_vtrxv1_3_default.json",
                    "debug": False,
                    "flx_G": "0",
                    "flx_d": "0",
                    "configInDB": False,
                    "test_mode": True,
                    "commToolName": "ic-over-netio",
                },
            },
            {
                "type": "opto_config",
                "name": "opto_config.json",
                "data": {
                    "Optoboard": {
                        "serial": "V2_008",
                        "version": 2,
                        "config_mode": "IC",
                        "FELIX": 1,
                        "link_group": 0,
                        "FPGA_USB_port": 0,
                        "PSU_USB_port": 1,
                        "debug": 0,
                        "I2C_master": 2,
                        "I2C_set": 1,
                        "SCLDriveMode": 0,
                        "FREQ": 2,
                    }
                },
            },
            [
                {
                    "type": "chip1_random",
                    "name": "chip1_random.json",
                    "data": {"RD53A": 1},
                }
            ],
            [
                {
                    "type": "chip1_random",
                    "name": "chip1_random.json",
                    "data": {"RD53A": 1},
                }
            ],
        ],
        [
            {
                "type": "opto_meta",
                "name": "opto_meta.json",
                "data": {
                    "serial": 303,
                    "vtrx_v": "1.3",
                    "ConfigurationDB": "http://localhost:5000",
                    "config_path": "/usr/local/lib/python3.9/site-packages/optoboard_felix/configs/optoboard2_lpgbtv1_gbcr2_vtrxv1_3_default.json",
                    "debug": False,
                    "flx_G": "0",
                    "flx_d": "0",
                    "configInDB": False,
                    "test_mode": True,
                    "commToolName": "ic-over-netio",
                },
            },
            {
                "type": "opto_config",
                "name": "opto_config.json",
                "data": {
                    "Optoboard": {
                        "serial": "V2_008",
                        "version": 2,
                        "config_mode": "IC",
                        "FELIX": 1,
                        "link_group": 0,
                        "FPGA_USB_port": 0,
                        "PSU_USB_port": 1,
                        "debug": 0,
                        "I2C_master": 2,
                        "I2C_set": 1,
                        "SCLDriveMode": 0,
                        "FREQ": 2,
                    }
                },
            },
            [
                {
                    "type": "chip1_random",
                    "name": "chip1_random.json",
                    "data": {"RD53A": 1},
                }
            ],
            [
                {
                    "type": "chip1_random",
                    "name": "chip1_random.json",
                    "data": {"RD53A": 1},
                }
            ],
        ],
    ],
]

reused_id = uuid4().hex
reuse_data = {
    "type": "root",
    "children": [
        {
            "type": "A",
            "children": [{"type": "A1"}, {"type": "A2", "id": reused_id, "children": [{"type": "a1"}, {"type": "a2"}, {"type": "a3"}]}],
        },
        {
            "type": "B",
            "children": [
                {
                    "reuse_id": reused_id
                    # this will add A2 as a subtree to B
                },
                {"type": "B2"},
            ],
        },
    ],
}


format_tree_data = "\nroot object:\n        config payload:\n                data: root_node \n                name: test\n        felix object:\n                felix payload:\n                        data: payload_fe\n                        name: test\n                lpgbt object:\n                        lpgbt payload:\n                                data: payload_lp\n                                name: test\n                        frontend object:\n                                frontend payload:\n                                        data: payload_fe\n                                        name: test"
format_tag_data = "author: pytest\nobjects:\nroot object:\n        config payload:\n                data: root_node \n                name: test\n        felix object:\n                felix payload:\n                        data: payload_fe\n                        name: test\n                lpgbt object:\n                        lpgbt payload:\n                                data: payload_lp\n                                name: test\n                        frontend object:\n                                frontend payload:\n                                        data: payload_fe\n                                        name: test\npayloads:\nconfig payload:\n        data: root_node \n        name: test"
