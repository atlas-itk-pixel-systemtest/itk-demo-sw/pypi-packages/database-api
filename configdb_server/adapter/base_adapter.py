from abc import ABC, abstractmethod
from sqlalchemy.orm import Session
from typing import Union
import uuid

from configdb_server.models.model import TagModel
from configdb_server.models.model import ObjectModel
from configdb_server.models.model import PayloadModel
from configdb_server.profiling import SQLAlchemyProfiling


class Connection:
    view: int
    id: uuid.UUID
    depth: int

    def __init__(self, id: uuid.UUID, view: int = 1, depth: int = 0):
        self.id = id
        self.view = view
        self.depth = depth


class BaseAdapter(ABC):
    # Basic CRUD

    profile: SQLAlchemyProfiling

    @abstractmethod
    def read_tag(self, read_session, name: str) -> TagModel:
        """
        Gets a tag from the database using its name

        Parameters
        ----------
        read_session: instance of a session for database access
        name: name of the tag

        Returns
        -------
        Python object representing the tag dataset
        """
        pass

    @abstractmethod
    def create_tag(
        self,
        write_session,
        name: str,
        type: str,
        objects: list[str] = [],
        payloads: list[str] = [],
        groups: list[str] = [],
        members: list[str] = [],
        author=None,
        id=None,
        comment=None,
        tag_latest=False,
    ) -> str:
        """
        Creates a tag in the database

        Parameters
        ----------
        write_session: instance of a session for database access
        name: name of the tag
        type: type of the tag
        objects: list of UUID strings of object datasets that should be included to the tag
        payloads: list of UUID strings of payload datasets that should be included to the tag
        groups: list of tag names that this tag should be included in
        members: list of tag names this tag should include
        author: author of the tag
        id: uuid hex-string for the tag
        comment: comment for the tag
        tag_latest: defines whether the tag tagged as latest or not

        Returns
        -------
        String containing the hex of the datasets UUID
        """
        pass

    # @abstractmethod
    # def update_tag(self, session, name: str, objects: list[str] = [], payloads: list[str] = [], metadata: list[str] = [], delete: bool = False) -> str:
    #     """
    #     Updates a tag in the database

    #     Parameters
    #     ----------
    #     name: name of the tag
    #     objects: list of UUID strings of object datasets that are included in the tag
    #     payloads: list of UUID strings of payload datasets that are included in the tag
    #     metadata: list of UUID strings of metadata datasets that should be included to the tag
    #     delete: defines whether to delete (and replace) existing objects and payload

    #     Returns
    #     -------
    #     String containing the hex of the datasets UUID
    #     """
    #     pass

    @abstractmethod
    def add_to_tag(
        self,
        write_session,
        name: str,
        objects: list[str] = [],
        payloads: list[str] = [],
        groups: list[str] = [],
        members: list[str] = [],
    ) -> str:
        """
        Adds payloads and objects to a tag

        Parameters
        ----------
        write_session: instance of a session for database access
        name: name of the tag
        objects: list of UUID strings of object datasets that should be added to the tag
        payloads: list of UUID strings of payload datasets that should be added to the tag
        groups: list of tag names that this tag should be included in
        members: list of tag names this tag should include

        Returns
        -------
        String containing the hex of the datasets UUID
        """
        pass

    @abstractmethod
    def remove_from_tag(
        self,
        write_session,
        name: str,
        objects: list[str] = [],
        payloads: list[str] = [],
        groups: list[str] = [],
        members: list[str] = [],
    ) -> str:
        """
        Removes payloads and objects from a tag

        Parameters
        ----------
        write_session: instance of a session for database access
        name: name of the tag
        objects: list of UUID strings of object datasets to remove
        payloads: list of UUID strings of payload datasets to remove
        groups: list of tag names that this tag should be included in
        members: list of tag names this tag should include

        Returns
        -------
        String containing the hex of the datasets UUID
        """
        pass

    @abstractmethod
    def read_object(self, read_session, id: str, view: int = 1) -> ObjectModel:
        """
        Gets a object from the database using its id

        Parameters
        ----------
        read_session: instance of a session for database access
        id: hex string of the objects uuid
        view: view of the connection

        Returns
        -------
        Object
        Python object representing the object dataset
        """
        pass

    @abstractmethod
    def create_object(
        self,
        write_session,
        type: str,
        children: list[Connection] = [],
        parents: list[Connection] = [],
        payloads: list[str] = [],
        tags: list[str] = [],
        id: str = None,
    ) -> str:
        """
        Creates a object in the database

        Parameters
        ----------
        write_session: instance of a session for database access
        type: type of the object
        parents: list of connections to parents with UUID strings of objects and their view
        children: list of connections to children with UUID strings of objects and their view
        payloads: list of UUID strings of payload datasets that should be included to the object
        tags: list of names of tag datasets that should be include the object
        id: uuid hex-string for the tag

        Returns
        -------
        String containing the hex of the datasets UUID
        """
        pass

    @abstractmethod
    def add_to_object(self, write_session, id: str, children: list[Connection] = [], payloads: list[Connection] = []) -> str:
        """
        Adds payloads and objects to an object

        Parameters
        ---------
        write_session: instance of a session for database access
        id: uuid hex-string for the object
        children: list of connections to children with UUID strings of objects and their view

        Returns
        -------
        String containing the hex of the datasets UUID
        """
        pass

    @abstractmethod
    def remove_from_object(self, write_session, id: str, children: list[str] = [], payloads: list[str] = []) -> str:
        """
        Removes payloads and objects from an object

        Parameters
        ----------
        write_session: instance of a session for database access
        id: uuid hex-string for the object
        children: list of UUID strings of object datasets to remove
        payloads: list of UUID strings of payload datasets to remove
        """
        pass

    @abstractmethod
    def update_object_payload(self, write_session, id: str, payload_id: str, new_id: str):
        """
        Replaces payload of an object with a new one

        Parameters
        ----------
        write_session: instance of a session for database access
        id: uuid hex-string for the object
        payload_id: uuid hex-string for the payload to replace
        new_id: uuid hex-string for the new payload
        """
        pass

    @abstractmethod
    def update_connections(self, write_session, connections: list[dict] = []):
        """
        Updates view of connection between two objects

        Parameters
        ----------
        write_session: instance of a session for database access
        connections: list of dicts containing parent, child and new view value
        """
        pass

    @abstractmethod
    def read_payload(self, read_session, id: str, meta: bool = False, format: bool = False) -> PayloadModel:
        """
        Gets a payload from the database using its id

        Parameters
        ----------
        read_session: instance of a session for database access
        id: hex string of the payloads uuid
        meta: defines whether the payload is a json or blob
        format: defines whether the data should be formatted (only works for json payloads)

        Returns
        -------
        Python object representing the payload dataset
        """
        pass

    @abstractmethod
    def create_payload(
        self,
        write_session,
        type: str,
        data: str,
        name: str = None,
        id: str = None,
        meta=False,
        tags: list[str] = [],
        objects: list[str] = [],
        encode: bool = True,
    ) -> str:
        """
        Creates a payload in the database

        Parameters
        ----------
        write_session: instance of a session for database access
        data: string containing the payload-data (e.g. json file)
        type: type of the payload
        name: name of the file saved in the payload-data
        id: uuid hex-string for the tag
        meta: defines whether the payload is saved as json or blob
        tags: list of names of tag datasets that should include the payload
        objects: list of UUID strings of object datasets that should include the payload
        encode: defines whether payload data has to be encoded

        Returns
        -------
        String containing the hex of the datasets UUID
        """
        pass

    @abstractmethod
    def update_payload(self, write_session, id, type="", data="", name="", meta=False, encode=True) -> None:
        """
        Creates a payload in the database

        Parameters
        ----------
        write_session: instance of a session for database access
        id: uuid hex-string of the payload
        data: new string containing the payload-data (e.g. json file)
        type: new type of the payload
        meta: defines whether the payload is a json or blob
        name: new name of the file saved in the payload-data

        Returns
        -------
        True
        """
        pass

    @abstractmethod
    def delete_tree(self, write_session, identifier: str):
        """
        Deletes datasets associated with a tag or root-payload from the database
        This should only be executed on the staging area db

        Parameters
        ----------
        write_session: instance of a session for database access
        identifier: name of the root tag or UUID of the root node

        Returns
        -------
        String containing the hex of the deleted tag/object
        """
        pass

    @abstractmethod
    def read_table(
        self,
        read_session,
        table: str,
        filter: str = "",
        name_filter: str = "",
        payload_filter: str = "",
        child_filter: str = "",
        offset: int = 0,
        limit: int = 0,
        order_by: str = "",
        asc=True,
        runkey_info: bool = False,
        payload_data: bool = False,
        depth: int = -1,
    ) -> list[dict]:
        """
        Method to retrieve all datasets from a specified table

        Parameters
        ----------
        read_session: instance of a session for database access
        table: table that should be listed
        filter: type to filter by
        name_filter: name to filter by
        payload_filter: type of associated payload to filter by (only works for direct associations)
        child_filter: type of associated object to filter by (only works for direct associations)
        offset: offset for the listed datasets
        limit: limit for the listed datasets
        order_by: table attribute by which to order the list
        asc: set to True for ascending order, False for descending order
        runkey_info: adds information about the runkey to each dataset
        payload_data: defines whether data of payload datasets should be included in the output
        depth: defines depth level to which the trees should be read (-1 for full tree)

        Returns
        -------
        List of dict containing datasets from the given table
        """
        pass

    @abstractmethod
    def insert(self, write_session, table: str, list: "list[dict]", encode: bool = True):
        """
        Method to insert multiple dataset at once

        Parameters
        ----------
        write_session: instance of a session for database access
        table: table that should be listed
        list: list with dictionaries that represent datasets
        encode: defines whether payload data has to be encoded
        """
        pass

    @abstractmethod
    def get_ancestors(self, read_session, id: str, depth_offset: int = 0) -> list[dict]:
        """
        Method to return all ancestors and their depth

        Parameters
        ----------
        read_session: instance of a session for database access
        id: uuid hex-string for the tag
        depth_offset: offset for the depth entry
        view: view of the connection

        Returns
        -------
        List of dict containing ancestors and their depth
        """
        pass

    @abstractmethod
    def get_descendants(self, read_session, id: str, depth_offset: int = 0) -> list[dict]:
        """
        Method to return all descendants and their depth

        Parameters
        ----------
        read_session: instance of a session for database access
        id: uuid hex-string for the tag
        depth_offset: offset for the depth entry
        view: view of the connection

        Returns
        -------
        List of dict containing descendants and their depth
        """
        pass

    @abstractmethod
    def read_object_tree(
        self,
        read_session,
        id: str,
        payload_data: bool = False,
        payload_filter: str = "",
        decode: bool = True,
        format: bool = False,
        depth: int = -1,
        view: int = 1,
    ) -> dict:
        """
        Method to retrieve a tree from an object

        Parameters
        ----------
        read_session: instance of a session for database access
        id: uuid of the root object of the (sub-)tree
        payload_data: adds data of payload to each dataset
        payload_filter: only include payloads which type contains this string
        decode: defines whether payload data should be decodeed
        format: defines whether the data should be formatted (only works for json payloads)
        depth: defines depth level to which the tree should be read (-1 for full tree)
        view: view of the tree (e.g. 3 will include stale connections)

        Returns
        -------
        Dict containing the requested object tree
        """
        pass

    @abstractmethod
    def read_tag_tree(
        self,
        read_session,
        name: str,
        payload_data: bool = False,
        payload_filter: str = "",
        decode: bool = True,
        format: bool = False,
        depth: int = -1,
        view: int = 1,
    ) -> dict:
        """
        Method to retrieve a tree from a tag

        Parameters
        ----------
        read_session: instance of a session for database access
        name: name of the tag
        payload_data: adds data of payload to each dataset
        payload_filter: only include payloads which type contains this string
        decode: defines whether payload data should be decodeed
        format: defines whether the data should be formatted (only works for json payloads)
        depth: defines depth level to which the tree should be read (-1 for full tree)
        view: view of the tree (e.g. 3 will include stale connections)

        Returns
        -------
        Dict containing the requested tag tree
        """
        pass

    def read_tree(
        self,
        read_session,
        identifier: str,
        payload_data: bool = False,
        payload_filter: str = "",
        decode: bool = True,
        format: bool = False,
        depth: int = -1,
        view: int = 1,
    ) -> dict:
        """
        Method to retrieve a tree from a tag

        Parameters
        ----------
        read_session: instance of a session for database access
        identifier: name of the tag or uuid of root node
        payload_data: adds data of payload to each dataset
        payload_filter: only include payloads which type contains this string
        decode: defines whether payload data should be decodeed
        format: defines whether the data should be formatted (only works for json payloads)
        depth: defines depth level to which the tree should be read (-1 for full tree)
        view: view of the tree (e.g. 3 will include stale connections)

        Returns
        -------
        Dict containing the requested tag tree
        """
        pass

    @abstractmethod
    def write_full_tree(self, write_session, data: "list[dict]", encode: bool = True, keep_ids: bool = True) -> str:
        """
        Method to create a full tree based on the nested data dict

        Parameters
        ----------
        write_session: instance of a session for database access
        data: list of dicts containing all children and payloads for the new tree
        encode: defines whether payload data has to be encoded
        keep_ids: defines whether ids should be kept or new ones should be created

        Returns
        -------
        String containing the hex of the root node UUID
        """
        pass

    @abstractmethod
    def write_lists(self, write_session, lists: "list[list]", encode: bool = True, keep_ids: bool = True) -> str:
        """
        Method to write data from lists

        Parameters
        ----------
        write_session: instance of a session for database access
        data: list of dicts containing all children and payloads for the new tree
        encode: defines whether payload data has to be encoded
        keep_ids: defines whether ids should be kept or new ones should be created

        Returns
        -------
        String containing the hex of the root node UUID
        """
        pass

    def read_lists(self, read_session, identifier: str, payload_data: bool = False, decode: bool = True, depth: int = -1, view: int = 1) -> dict:
        """
        Method to read a runkey in list format

        Parameters
        ----------
        read_session: instance of a session for database access
        identifier: name of the tag or uuid of root node
        payload_data: adds data of payload to each dataset
        decode: defines whether payload data should be decodeed
        depth: defines depth level to which the tree should be read (-1 for full tree)
        view: view of the tree (e.g. 3 will include stale connections)

        Returns
        -------
        Dict containing the requested tag tree
        """
        pass

    def search_for_config(self, read_session, identifier: str, object_type: str = "", config_type: str = "", search_dict: dict = {}):
        """
        Method to get the specific payloads of an object of a specific type with specific metadata

        Parameters
        ----------
        read_session: instance of a session for database access
        identifier: name of the root tag or UUID of the root node
        object_type: type of the object to search for
        config_type: type of the config to search for
        search_dict: dict containing the metadata values to search for

        Returns
        -------
        list of objects and their payloads
        """
        pass

    @abstractmethod
    def search_in_tag(
        self,
        read_session,
        name: str,
        payload_types: list[str] = [],
        object_ids: list[str] = [],
        search_dict: dict = None,
        payload_data: bool = False,
        order_by_object: bool = False,
    ) -> Union[list, dict]:
        """
        Gets all payloads with a specific type and object metadata from a tag

        Parameters
        ----------
        read_session: instance of a session for database access
        name: name of the tag
        payload_types: list of types to filter by
        search_dict: dict containing the metadata values to filter by


        Returns
        -------
        List of payloads
        """
        pass

    def search_for_subtree(
        self,
        read_session,
        identifier: str,
        object_type: str = "",
        search_dict: dict = {},
        payload_data: bool = False,
        decode: bool = True,
        depth: int = -1,
        view: int = 1,
    ):
        """
        Method to get the subtrees with a root node of a specific type with specific metadata

        Parameters
        ----------
        read_session: instance of a session for database access
        identifier: name of the root tag or UUID of the root node
        object_type: type of the object to search for
        search_dict: dict containing the metadata values to search for
        payload_data: adds data of payload to each dataset
        decode: defines whether to decode the data of the payloads
        depth: defines depth level to which the tree should be read (-1 for full tree)
        view: view of the tree (e.g. 3 will include stale connections)

        Returns
        -------
        list of subtrees
        """
        pass

    # Database functionality
    @abstractmethod
    def create_owner_session(self) -> Session:
        pass

    @abstractmethod
    def create_read_session(self) -> Session:
        pass

    @abstractmethod
    def create_write_session(self) -> Session:
        pass

    @abstractmethod
    def database_running(self) -> bool:
        """
        Checks whether the database is running

        Returns
        -------
        True: if database is running
        False: if database is not running
        """
        pass

    @abstractmethod
    def database_exists(self) -> bool:
        """
        Checks whether the database exists

        Returns
        -------
        True: if database exists
        False: if database does not exist
        """
        pass

    @abstractmethod
    def create_database(self):
        """
        Creates the database in case of sqlite database, will not create server based databases (these have to be started manually)
        """
        pass

    @abstractmethod
    def database_migration_status(self, read_session) -> bool:
        """
        Checks whether all the tables in the database exist (with the correct attributes)

        Parameters
        ----------
        read_session: instance of a session for database access

        Returns
        -------
        True: if database schema is up to date
        False: if database schema is not up to date
        """
        pass

    @abstractmethod
    def upgrade_database(self):
        """
        Upgrades the database schema
        """
        pass

    @abstractmethod
    def downgrade_database(self):
        """
        Downgrades the database schema
        """
        pass

    @abstractmethod
    def clear_database(self):
        """
        Deletes all datasets in a database
        """
        pass
