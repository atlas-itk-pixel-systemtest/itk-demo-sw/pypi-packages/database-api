from configdb_server.database_tools import Database, Backends
import json
import cProfile
import pstats
import io

def main():
    #stage_db = Database(Backends.SQLALCHEMY_MARIADB, "configdb:test@localhost:3307/configdb")
    db = Database(Backends.SQLALCHEMY_MARIADB, "configdb:test@localhost:32818/configdb")

    # stage_db.init_database()
    # db.init_database()

    # db.backend.clear_database()
    # stage_db.backend.clear_database()


    session = db.backend.create_write_session()
    profile(db, session)
    session.close()

def profile(db: Database, session):
    profiler = cProfile.Profile()
    profiler.enable()

    # Call the delete_tree method
    res2 = db.get_all("tag")

    profiler.disable()
    s = io.StringIO()
    sortby = 'tottime'
    ps = pstats.Stats(profiler, stream=s).sort_stats(sortby)
    ps.print_stats()
    
    with open('profiling_results.txt', 'w') as f:
        f.write(s.getvalue())

    return res2


if __name__ == "__main__":
    main()

